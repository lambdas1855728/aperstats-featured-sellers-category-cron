class Category{
    
    id_parent;
    id_shop_default;
    level_depth;
    nleft;
    nright;
    active;
    date_add;
    date_upd;
    position;
    is_root_category;

    constructor(idParent,position){
        const hoy = new Date();
        
        this.id_parent = idParent;
        this.id_shop_default = 1;
        this.level_depth = 1;
        this.nleft = 0;
        this.nright = 0;
        this.active = 1;
        this.date_add = hoy.toISOString().slice(0, 10).replace('T', ' ');
        this.date_upd = hoy.toISOString().slice(0, 10).replace('T', ' ');
        this.position = position;
        this.is_root_category = 0;
    }
}

module.exports = Category;