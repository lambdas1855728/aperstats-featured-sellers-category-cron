const StoreRepository = require('../Repository/StoreRepository');
const DynamoRepository = require('../Repository/DynamoRepository');
const Category = require('../Entity/Category');
const CategoryLang = require('../Entity/CategoryLang');
const CategoryShop = require('../Entity/CategoryShop');
const https = require('https');

class FeaturedSellersService{
    storeRepository;
    dynamoRepository;
    idParent;
    
    constructor(){
        this.storeRepository = new StoreRepository();
        this.dynamoRepository = new DynamoRepository();
        this.idParent = process.env.id_parent_category;
    }
    
    async procesar(){
        await this.storeRepository.init();
        this.featureActive = (await this.dynamoRepository.getFeature('featuredSellersCategory')).Items[0].feature_active;
        
        if (Number(this.featureActive) == 0 ){
            await this.storeRepository.updateActiveParentCategory(this.idParent, 0);
            return 'OK, ninguna Categorias actualizada por efecto desactivado ';
        }
        const idCategories = (await this.storeRepository.getIdCategoriesByIdParent(this.idParent)).map(r => r.id_category).join(',');
        if (idCategories.length != 0)
            await this.storeRepository.deleteCategoriesById(idCategories);
        const globalStats = await this.dynamoRepository.traerGlobalStats();
        const globalStatsIdSuppliers = globalStats["Items"].filter(a=>a.global_color == 'green' && a.active_features == true).sort((a, b) => (Number(a.score) < Number(b.score)) ? 1 : -1).map(a => a.id_supplier).join(',');
        if (globalStatsIdSuppliers != ''){
            const suppliers = await this.storeRepository.getSuppliersById(globalStatsIdSuppliers);
            let position = 0;
            let cant = 0;
            for (const s of suppliers){
                let category = new Category(this.idParent,position);
                let categoryLang = new CategoryLang(s);
                let categoryShop = new CategoryShop();
                await this.storeRepository.guardarCategory(category,categoryLang,categoryShop);
                cant=cant+1
                position = position+1;
            }
            await this.clearCacheMenu();
            await this.storeRepository.updateActiveParentCategory(this.idParent, 1);
            await this.storeRepository.end();
            return 'OK Categorias actualizadas '+cant;
        }
        await this.storeRepository.updateActiveParentCategory(this.idParent, 0);
        await this.storeRepository.end();
        return 'OK, ninguna Categorias actualizada ';
    }
    
    async clearCacheMenu () {
        await this.storeRepository.updateDateZMenu();
        const result = await this.storeRepository.getAperDataURL();
        if (result && result[0].value) {
            const url = result[0].value;
            new Promise((resolve, reject) => {
                const req = https.get(`${url}/cache/clear`, (res) => {
                    let responseBody = '';
                    res.on('data', (chunk) => {
                        responseBody += chunk;
                    });
                    res.on('end', () => {
                        resolve(JSON.parse(responseBody));
                    });
                });
                req.on('error', (err) => {
                    reject(err);
                });
                req.end();
            });
        }
    }
    
}

module.exports = FeaturedSellersService;
