class UtilsServices {

  objectToQueryString = (obj) => {
    var str = [];
    for (var p in obj)
      if (obj.hasOwnProperty(p))
        str.push(p + "=" + this.valueFormat(obj[p]));
    return str.join(", ");
  };

  valueFormat = (val) => {
    if (isNaN(val)) return '\'' + val + '\'';
    return val;
  }

  catchError = (name) => {
    switch (name) {
      case "InvalidSignatureException":
        console.error('Invalid Signature Exception');
        break;
      case "ResourceNotFoundException":
        console.error('Resource Not Found Exception');
        break;
      case "FooServiceException":
        console.error('Foo Service Exception');
        break;
      default:
        throw new Error('Code error');
    }
  }

}
module.exports = UtilsServices;