const AWS = require('aws-sdk');
//require('dotenv').config();
const UtilsService = require('../Services/UtilsServices');

class DynamoRepository {

    db;
    globalTable;
    featuresTable;
    utils;

    constructor() {
        if (process.env.NODE_ENV === 'dev') {
            this.db = new AWS.DynamoDB.DocumentClient({
                region: process.env.REGION,
                endpoint: process.env.DYNAMODB_ENDPOINT,
                credentials: {
                    accessKeyId: process.env.DYNAMODB_ACCESS_KEY_ID,
                    secretAccessKey: process.env.DYNAMODB_SECRET_ACCESS_KEY
                }
            });
        } else {
            this.db = new AWS.DynamoDB.DocumentClient({
                region: process.env.REGION,
            });
        }
        this.featuresTable = process.env.table_features;
        this.globalTable = process.env.table_global;
        this.utils = new UtilsService();
    }

    getFeature = async (feature_name) => {
        console.log('Do getFeature()');
        const params = {
            TableName: this.featuresTable,
            IndexName: "feature_name-index",
            KeyConditionExpression: "feature_name = :feature_name",
            ExpressionAttributeValues: {
                ":feature_name": feature_name
            }
        };

        try {
            return await this.db.query(params).promise();
        } catch ({ name }) {
            this.utils.catchError(name);
        };
    }

    traerGlobalStats = async () => {
        console.log('Do traerGlobalStats()');
        const params = {
            TableName: this.globalTable,
        };

        try {
            return await this.db.scan(params).promise();
        } catch ({ name }) {
            this.utils.catchError(name);
        };
    }
}
module.exports = DynamoRepository;