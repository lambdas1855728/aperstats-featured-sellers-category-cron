
const AWS = require('aws-sdk');
//require('dotenv').config(); 
const { Sequelize, QueryTypes } = require('sequelize');
const UtilsServices = require('./../Services/UtilsServices');

class StoreRepository {

    sequelize;
    secrets;
    secretManager;

    init = async () =>{
        if(process.env.NODE_ENV=='dev'){
            this.sequelize = new Sequelize('database', null, null, {
                dialect: 'mysql',
                replication: {
                read: [
                    { 
                        host:     process.env.read_db_host,
                        port:     process.env.read_db_port,
                        database: process.env.read_db_name,
                        username: process.env.read_db_username,
                        password: process.env.read_db_password
                    },
                ],
                write: 
                    { 
                        host:     process.env.write_db_host,
                        port:     process.env.write_db_port,
                        database: process.env.write_db_name,
                        username: process.env.write_db_username,
                        password: process.env.write_db_password
                    },
                
                },
                pool: { 
                    max: Number(process.env.pool_max_connections), 
                    idle: 30000
                },
            });
        }else{
            this.secretManager = new AWS.SecretsManager({ region: process.env.REGION });
            this.secrets  = await this.getSecrets(process.env.secrets);
            this.sequelize = new Sequelize('database', null, null, {
                dialect: 'mysql',
                replication: {
                read: [
                    { 
                        host:     this.secrets.hostro,
                        port:     this.secrets.port,
                        database: process.env.read_db_name,
                        username: this.secrets.username,
                        password: this.secrets.password
                    },
                ],
                write: 
                    { 
                        host:     this.secrets.host,
                        port:     this.secrets.port,
                        database: process.env.write_db_name,
                        username: this.secrets.username,
                        password: this.secrets.password
                    },
                
                },
                pool: { 
                    max: Number(process.env.pool_max_connections), 
                    idle: 30000
                },
            });

        }   

    }

    getSecrets = async (id) => {
         return new Promise((resolve, reject) => {
            this.secretManager.getSecretValue({ SecretId: id }, (err, result) => {
                if (err) reject(err);
                else resolve(JSON.parse(result.SecretString));
            });
        });
    };
 
    getIdCategoriesByIdParent = async (idParent) => {
        const sql = `select id_category from ps_category where id_parent=${idParent}`;
        return await this.sequelize.query(sql, {type: QueryTypes.SELECT});
    };
    
    updateActiveParentCategory = async (idParent, active) => {
        const sql = `update ps_category set active = ${active} where id_category=${idParent}`;
        await this.sequelize.query(sql, {type: QueryTypes.UPDATE});
    };

    deleteCategoriesById = async (idCategorias) => {
        let sqls = [];
        sqls.push('delete from ps_category_lang where id_category in (' + idCategorias + ')');
        sqls.push('delete from ps_category_shop where id_category in (' + idCategorias + ') ');
        sqls.push('delete from ps_category where id_category in (' + idCategorias + ')');
        sqls.forEach( async sql => {
            await this.sequelize.query(sql, {type: QueryTypes.DELETE});
        })
    };

    getCategoryByName = async (name) => {
        const sql = `select a.id_category 
        from ps_category_lang a 
        inner join ps_category b
        on a.id_category = b.id_category
        where a.name='${name}' and b.active = '1' limit 1`;
        return await this.sequelize.query(sql, {type: QueryTypes.SELECT});
    }

    getSuppliersById = async (ids) => {
        const sql = `select * from ps_supplier where id_supplier in (${ids}) order by FIND_IN_SET(id_supplier,'${ids}');`;
        return await this.sequelize.query(sql, {type: QueryTypes.SELECT});
    };

    guardarCategory = async (category, categoryLang, categoryShop) => {
        const utils = new UtilsServices();
        const trans = await this.sequelize.transaction();

        try {
            let result = await this.sequelize.query('insert into ps_category set ' + utils.objectToQueryString(category), {type: QueryTypes.INSERT});
            categoryLang.id_category = result[0];
            categoryShop.id_category = result[0];
            await this.sequelize.query('insert into ps_category_lang set ' + utils.objectToQueryString(categoryLang), {type: QueryTypes.INSERT});
            await this.sequelize.query('insert into ps_category_shop set ' + utils.objectToQueryString(categoryShop), {type: QueryTypes.INSERT});
            await trans.commit();
            return 'ok';
        } catch (error) {
            console.error(error);
            await trans.rollback();
        }
    }

    updateDateZMenu = async ()=>{
        const sql = `update ps_configuration set value = NOW() where name = 'ZONEMEGAMENU_DATE_CACHE';`;
        await this.sequelize.query(sql, {type: QueryTypes.UPDATE});
    }
    
    getAperDataURL = async () => {
        const sql = "select value from ps_configuration where name = 'API_STORE_MENU_URL';";
        return await this.sequelize.query(sql, {type: QueryTypes.SELECT});
    }

    end = async () => {
        await this.sequelize.close();
    }
}
module.exports = StoreRepository;
