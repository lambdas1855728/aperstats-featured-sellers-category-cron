const FeaturedSellersService = require('./src/Services/FeaturedSellersService');

exports.handler = async () => {
   let service = new FeaturedSellersService();
   return await service.procesar();
};

